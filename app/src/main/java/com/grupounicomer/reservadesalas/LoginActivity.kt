package com.grupounicomer.reservadesalas

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()

        val user = mAuth!!.currentUser

        if (user != null){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        button_login.setOnClickListener {
            val username = text_input_username.editText!!.text.toString().trim()
            val password = text_input_password.editText!!.text.toString().trim()

            if (username.equals("") || password.equals("")){
                if (username.equals("")) text_input_username.error = "El usuario no puede estar vacío"
                if (password.equals("")) text_input_password.error = "La contraseña no puede estar vacía"
            }else{
                login_loading_screen.visibility = View.VISIBLE
                login_form.visibility = View.GONE
                mAuth!!.signInWithEmailAndPassword(username,password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            // login_loading_screen.visibility = View.GONE
                            // login_form.visibility = View.VISIBLE
                            // Toast.makeText(this, "USER READY", Toast.LENGTH_LONG).show()

                            val userLoggedIn = mAuth!!.currentUser


                            val sharedPreferences = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
                            val editor: SharedPreferences.Editor = sharedPreferences.edit()
                            editor.putString(USER_LOGGED_IN_KEY, userLoggedIn!!.email)
                            editor.apply()

                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            login_loading_screen.visibility = View.GONE
                            login_form.visibility = View.VISIBLE
                            Snackbar.make(button_login, "Usuario y/o contraseña incorrectos", Snackbar.LENGTH_LONG).show()


                        }
                    }

                val x = 1+1

                val y = 2+1 
            }
        }
    }
}
