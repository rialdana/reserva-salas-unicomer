package com.grupounicomer.reservadesalas

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.grupounicomer.reservadesalas.fragments.HistorialReservasFragment
import com.grupounicomer.reservadesalas.fragments.MisReservasFragment
import com.grupounicomer.reservadesalas.fragments.ReservasActivasFragment
import com.grupounicomer.reservadesalas.fragments.TestFragment

class MainActivity : AppCompatActivity(),
    MisReservasFragment.OnFragmentInteractionListener,
    TestFragment.OnFragmentInteractionListener,
    HistorialReservasFragment.OnFragmentInteractionListener,
    ReservasActivasFragment.OnFragmentInteractionListener {

    private var mAuth: FirebaseAuth? = null
    var username: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val preferences = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
        username = preferences.getString(USER_LOGGED_IN_KEY, "NO")
        Log.i("USERNAME", "USER EMAIL: $username")

        mAuth = FirebaseAuth.getInstance()

        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_misReservas -> {
                    val misReservas = MisReservasFragment.newInstance()
                    openFragment(misReservas, R.id.bottomNavigationContainer)
                }
                R.id.menu_reservarSala -> {
                    //val reservarSala = ReservarSalaFragment.newInstance()
                    //openFragment(reservarSala, R.id.bottomNavigationContainer)
                }
                R.id.menu_salasDisponibles -> {
                    //val salasDisponibles = SalasDisponiblesFragment.newInstance()
                    //openFragment(salasDisponibles, R.id.bottomNavigationContainer)
                }
            }
            true
        }
        bottomNavigation.selectedItemId = R.id.menu_misReservas
    }

    override fun logOut() {
        val sharedPreferences = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
        val sharedPref = sharedPreferences?.edit()
        sharedPref!!.clear()
        sharedPref.apply()

        mAuth!!.signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    fun openFragment(fragment: Fragment, containerID: Int) {
        var transaction = supportFragmentManager
            .beginTransaction()
            .replace(containerID, fragment, "")
        transaction.commit()
    }
}
