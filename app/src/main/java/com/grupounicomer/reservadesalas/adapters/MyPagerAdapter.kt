package com.grupounicomer.reservadesalas.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.grupounicomer.reservadesalas.fragments.HistorialReservasFragment
import com.grupounicomer.reservadesalas.fragments.ReservasActivasFragment

class MyPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position){
            0 -> ReservasActivasFragment.newInstance()
            else -> HistorialReservasFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Activas"
            else -> {
                return "Historial"
            }
        }
    }

}