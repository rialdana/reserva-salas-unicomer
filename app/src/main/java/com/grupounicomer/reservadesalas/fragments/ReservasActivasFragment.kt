package com.grupounicomer.reservadesalas.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.grupounicomer.reservadesalas.APP_NAME
import com.grupounicomer.reservadesalas.MainActivity
import com.grupounicomer.reservadesalas.R
import com.grupounicomer.reservadesalas.USER_LOGGED_IN_KEY
import kotlinx.android.synthetic.main.fragment_reservas_activas.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ReservasActivasFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ReservasActivasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ReservasActivasFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null
    private var dbInstance: FirebaseFirestore? = null
    var username: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reservas_activas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dbInstance = FirebaseFirestore.getInstance()
        val preferences = this.activity!!.getSharedPreferences(APP_NAME, Context.MODE_PRIVATE)
        username = preferences.getString(USER_LOGGED_IN_KEY, "NO")
        Log.i("USERNAME", "USER EMAIL: $username")

        button_logout.setOnClickListener {
            // listener!!.logOut()
            dbInstance!!.collectionGroup("Reservas").whereEqualTo("Usuario",username).get()
                .addOnSuccessListener {
                    Log.i("INFO", it.toString())
                }
                .addOnFailureListener {
                    Log.e("INFO", "ERROR", it)
                }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun logOut()
        // fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance() = ReservasActivasFragment()
    }
}
